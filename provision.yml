---
- name: provision vm from cloud-init enabled image
  become: yes
  hosts: localhost
  gather_facts: no

  vars:
    host_name: "{{ hostname | default('test') }}"
    storage_path: '/home/esc_type/Other/store/'
    image_path: "{{ playbook_dir }}/Image"
    image_newsize: "{{ image_resize }}"
    cloud_init_path: "{{ playbook_dir }}/cloudinit"

  vars_files:
    - "{{ size | default('vars/C8-micro.yml') }}"

  tasks:
    - name: Ensure that disk storage is there
      file:
        path: '{{ storage_path }}'
        state: directory
        mode: '0755'

    - name: Check if image already exists
      stat:
        path: '{{ storage_path }}/{{ host_name }}.qcow2'
      register: stat_result

    - name: Stop execution if image exists
      fail:
        msg: File exists!
      when: stat_result.stat.exists

    - name: Instantiate image
      synchronize:
        src: '{{ image_path }}/{{ base_image }}'
        dest: '{{ storage_path }}/{{ host_name }}.qcow2'
        archive: yes

    - name: Create '{{ cloud_init_path }}'
      file:
        name: '{{ cloud_init_path }}'
        state: directory
        mode: '0755'

    - name: Copy "{{ cloud_init_path }}/user-data"
      template:
        src: templates/user-data.j2
        dest: "{{ cloud_init_path }}/user-data"
        mode: '0644'

    - name: Create "{{ cloud_init_path }}/meta-data"
      file:
        path: "{{ cloud_init_path }}/meta-data"
        state: touch
        mode: '0644'

    - name: Generate new meta-data instance-id
      lineinfile:
        path: '{{ cloud_init_path }}/meta-data'
        line: 'instance-id: {{ host_name }}'
        state: present

    - name: Generate new meta-data local-hostname
      lineinfile:
        path: '{{ cloud_init_path }}/meta-data'
        line: 'local-hostname: {{ host_name }}'
        insertafter: EOF

    - name: Resize disk image (cloud-image resizes partitions to match new size)
      command: qemu-img resize {{ storage_path }}/{{ host_name }}.qcow2 {{ image_newsize }}
      when: image_resize is defined

    - name: Import VM
      command: virt-install \
               --name '{{ host_name }}' \
               --memory '{{ memory }}' \
               --vcpus '{{ vcpus }}' \
               --memballoon virtio \
               --import \
               --disk '{{ storage_path }}/{{ host_name }}.qcow2',format=qcow2,bus=virtio \
               --cloud-init user-data='{{ cloud_init_path }}/user-data',meta-data='{{ cloud_init_path }}/meta-data' \
               --os-type=linux \
               --os-variant=rhel8.4 \
               --noautoconsole

    - name: Remove old meta-data file
      file:
        path: '{{ cloud_init_path }}/meta-data'
        state: absent

    - name: Remove old user-data file
      file:
        path: '{{ cloud_init_path }}/user-data'
        state: absent

    - name: Remove temporary cloudinit directory
      file:
        path: '{{ cloud_init_path }}'
        state: absent

    - name: Wait till domain is ready (30 sec)
      wait_for:
        timeout: 30

    - name: Get domain ip address
      shell: virsh net-dhcp-leases default | grep '{{ host_name }}' | awk '{ print $5 }' | awk -F/ '{ print $1 }'
      register: dom_ip_address

    - name: Print address
      debug:
        msg: "{{ host_name }} address is {{ dom_ip_address.stdout }}"
